# Part 3 - Making new predictions
import numpy as np
from keras.preprocessing import image
from keras.models import load_model

classifier = load_model('model_2_classes.h5')
test_image = image.load_img('dataset/test_set/tunnel/Photo124____20180807153717_55.jpg', target_size = (256, 256))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = classifier.predict(test_image)
print(result)
