from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator


#Architecture du modèles

classifier = Sequential()

classifier.add(Convolution2D(32,3,3,input_shape = (256,256,3),activation='relu'))

classifier.add(MaxPooling2D(pool_size = (2,2)))

classifier.add(Flatten())

classifier.add(Dense(output_dim = 128, activation = 'relu'))
classifier.add(Dense(output_dim = 2, activation = 'sigmoid'))

classifier.compile(optimizer = 'adam', loss='mean_squared_error', metrics = ['accuracy'])

# Data augmentation

train_datagen = ImageDataGenerator (
                validation_split = 0.2,
                rescale = 1./255,
                shear_range = 0.2,
                zoom_range = 0.2,
                horizontal_flip = True
)

test_datagen = ImageDataGenerator(rescale=1./255)

training_set = train_datagen.flow_from_directory(
    'dataset/training_set',
    target_size = (256,256),
    batch_size = 32,
    class_mode = 'categorical',
    subset = 'training'
)

test_set = test_datagen.flow_from_directory(
    'dataset/training_set',
    target_size = (256,256),
    batch_size = 32,
    class_mode = 'categorical',
    subset = 'validation'
)

classifier.fit_generator(training_set,steps_per_epoch = 80, epochs = 10, validation_data = test_set,validation_steps = 10)

from keras.models import load_model

classifier.save('model_2_classes.h5')